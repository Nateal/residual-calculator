// vue.config.js
module.exports = {
    //FOR CLOUD RUN
    publicPath: '/',
    devServer: {
        disableHostCheck: true,
        proxy: {
            // change xxx-api/login => mock/login
            // detail: https://cli.vuejs.org/config/#devserver-proxy
            "/api" : {

                    target: 'https://new-los-api-wivlpotrwa-an.a.run.app/v1/newlos',
                    pathRewrite: { '^/api': '' },
                    changeOrigin: true,
                    secure: false

            }
        }
    },
    configureWebpack: {
        output: { 
            filename: '[name].[hash].bundle.js' 
        } 
    }
}
