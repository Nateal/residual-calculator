import Vue from 'vue';

import App from './App'

import axios from 'axios'
import VueToast from 'vue-toast-notification';


import { BootstrapVue, IconsPlugin, BootstrapVueIcons } from 'bootstrap-vue';


import GoogleSignInButton from 'vue-google-signin-button-directive'


Vue.prototype.axios = axios
Vue.use(VueToast);
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(BootstrapVueIcons);


const eventsHub = new Vue({
  el: '#app',
  GoogleSignInButton,
 
  render: h => h(App),
})

Vue.config.productionTip = false
