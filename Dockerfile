#build stage
FROM node:14.7.0-alpine3.10 as build-stage
WORKDIR /app
COPY package*.json ./
CMD ["npm", "install"]
COPY . .

 #test
# CMD ["npm", "rebuild", "node-sass"]
run npm rebuild node-sass

RUN npm run build

#production
FROM nginx:1.19.1-alpine as production-stage
COPY --from=build-stage /app/dist /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d
EXPOSE 80
CMD [ "nginx", "-g", "daemon off;" ]
